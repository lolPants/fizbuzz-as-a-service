# Fizzbuzz as a Service (FBaaS) ![](https://gitlab.com/lolPants/fizbuzz-as-a-service/badges/master/build.svg)
_Fucking hell I need to stop._

## Usage
`GET` https://fizzbuzz-as-a-service.herokuapp.com/api/v1.0/1,100  
Returns a `text/event-stream` for the requested range.

## Credit
Built by [Jack Baron](https://www.jackbaron.com)
