/**
 * FizzBuzz Generator
 * @param {number} start Start of FizzBuzz
 * @example
 * // Setup generator and start a loop
 * let gen = fizzBuzz()
 * for (let i = 0; i < 100; i++) {
 *   console.log(gen.next().value)
 * }
 */
function* fizzBuzz (start = 1) { // eslint-disable-line
  while (true) {
    let out = ''

    if (start % 3 === 0) out += 'Fizz'
    if (start % 5 === 0) out += 'Buzz'
    yield out === '' ? start : out
    start++
  }
}

module.exports = fizzBuzz
