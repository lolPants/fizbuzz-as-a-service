// Package Dependencies
const express = require('express')
const cors = require('cors')

// Local Dependencies
const fizzBuzz = require('./FizzBuzz')

// Setup app and middleware
const app = express()
app.use(cors())

// Redirect root to GitLab Repo
app.get('/', (req, res) => {
  res.redirect('https://gitlab.com/lolPants/fizbuzz-as-a-service')
})

app.get('/api/v1.0/:range', (req, res) => {
  /**
   * @type {string[]}
   */
  let ranges = req.params.range.split(',').filter(x => x !== '')
  // Check for two range arguments
  if (ranges.length < 2) return res.sendStatus(400)

  // Assign
  let start = parseInt(ranges[0])
  let end = parseInt(ranges[1])

  // Check for a valid range
  if (start >= end) return res.sendStatus(400)

  // Readable Stream
  res.writeHead(200, { 'Content-Type': 'text/event-stream' })

  // Stream FizzBuzz content
  let fizzbuzz = fizzBuzz(start)
  for (let i = 0; i <= end - start; i++) {
    res.write(`${fizzbuzz.next().value}\n`)
  }
  res.end()
})

// Listen
app.listen(process.env.PORT || 3000)
